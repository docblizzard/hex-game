from tkinter import *

root = Tk()
root.geometry("1200x650")
cnv = Canvas(root, bg ="white", height = 1100, width = 1100)

game_over = FALSE
turn = 0
hex_board = []
mb = Menu(root)
filemenu = Menu(mb, tearoff=0)

mb.add_cascade(label="Option", menu=filemenu)

def reset():
    print("Reseting")
    for y in range(11):
        for x in range(11):
            color = cnv.itemcget(hex_board[y][x], "fill")
            print(color)
            cnv.itemconfigure(hex_board[y][x], fill = "white")
    print(hex_board)
filemenu.add_command(label="Reset", command= reset)

def onclick(event):
    global turn
    item = cnv.find_overlapping(event.x, event.y, event.x, event.y )

    color = cnv.itemcget(item, "fill")
    if color == "white":
        if turn == 0:
            cnv.itemconfigure(item, fill = "#034efc")
            turn = 1
        else :
            cnv.itemconfigure(item, fill="#8f0707")
            turn = 0

# 11 * 11 hexagone
for y in range(11):
    row = []
    for x in range(11):
        case = cnv.create_polygon(40+(y*30)+(60*x), 75 + (y * 45), 70+(y*30)+(60*x), 60+(y *45), 100+(y*30)+(60*x), 75+(y *45), # 3 premier points
                           100+(y*30)+(60*x), 105+(y * 45), 70+(y*30)+(60*x), 120+(y *45), 40+(y*30)+(60*x), 105+(y *45), 40+(y*30)+(60*x), 75+(y *45),
                           width = 2, fill = "white", outline = "black")
        row.append(case)
        print(len(hex_board))

        # Decoration
        if y == 0:
            cnv.create_line(40 + (y * 30) + (60 * x), 70 + (y * 45), 70 + (y * 30) + (60 * x), 55 + (y * 45),
                           100 + (y * 30) + (60 * x), 70 + (y * 45),
                             width =4,  fill = "#034efc")
        if x == 0:
            cnv.create_line(65 + (y * 30) + (60 * x), 123 + (y * 45), 35 + (y * 30) + (60 * x), 108 + (y * 45), 35 + (y * 30) + (
                        60 * x), 78 + (y * 45),
                            width =4,  fill = "#8f0707")
        if y == 10:
            cnv.create_line(100+(y*30)+(60*x), 110+(y * 45), 70+(y*30)+(60*x), 125+(y *45), 40+(y*30)+(60*x), 110+(y *45),
                           width = 4, fill = "#034efc")
        if x == 10:
            cnv.create_line(75+(y*30)+(60*x), 57+(y *45), 105+(y*30)+(60*x), 72+(y *45),
                           105+(y*30)+(60*x), 102+(y * 45),
                            width=4, fill="#8f0707")

    hex_board.append(row)


all_nodes = []
for x in range(11):
    for y in range(11):
        all_nodes.append([x, y])

def neighbors(node):
    dirs = [[1, 0], [0, 1], [-1, 0], [0, -1], [1, -1], [-1, 1]]
    result = []
    for dir in dirs:
        neighbor = [node[0] + dir[0], node[1] + dir[1]]
        if neighbor in all_nodes:
            result.append(neighbor)
    return result


cnv.bind('<Button-1>', onclick)
root.config(menu=mb)
cnv.pack()
root.mainloop()

